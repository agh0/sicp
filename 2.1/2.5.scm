(define (cons a b)
  (* (expt 2 a)
     (expt 3 b)))

(define (divs-count num divisor)
  (define (iter num counter)
    (if (= (remainder num divisor) 0)
        (iter (/ num divisor) (+ 1 counter))
        counter))
  (iter num 0))

(define (car z) (divs-count z 2))

(define (cdr z) (divs-count z 3))


;(display (car (cons 5 8)))
;(newline)

;(display (cdr (cons 1 9)))
;(newline)
