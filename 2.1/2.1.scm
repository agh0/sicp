(load "chapter.scm")

(define (make-rat n d)
  (let ((sign (if (< d 0) (- 1) 1))
        (g (gcd n d)))
    (cons (* sign (/ n g)) (* sign (/ d g)))))

(print-rat (make-rat 3 6))
(print-rat (make-rat 3 -6))
(print-rat (make-rat -3 6))
(print-rat (make-rat -3 -6))

