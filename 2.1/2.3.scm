(load "2.2.scm")

(define (rect-perimeter rect)
  (* 2 (+ (rect-width rect)
          (rect-height rect))))

(define (rect-area rect)
  (* (rect-width rect)
     (rect-height rect)))


; 1 repr
(define (rect-width rect)
  (- (x-point (br-point rect))
     (x-point (tl-point rect))))

(define (rect-height rect)
  (- (y-point (tl-point rect))
     (y-point (br-point rect))))

(define make-rect cons)

(define tl-point car)

(define br-point cdr)


(define tl (make-point 0 10))
(define br (make-point 8 0))
(define rect (make-rect tl br))

(display (rect-perimeter rect))
(newline)

(display (rect-area rect))
(newline)


; 2 repr
(define (rect-width rect)
  (car (cdr rect)))

(define (rect-height rect)
  (cdr (cdr rect)))

(define (make-rect tl width height)
  (cons tl (cons width height)))

(define tl (make-point 0 10))
(define rect (make-rect tl 8 10))

(display (rect-perimeter rect))
(newline)

(display (rect-area rect))
(newline)
