(load "../common.scm")

(define make-segment cons)

(define start-segment car)

(define end-segment cdr)

(define make-point cons)

(define x-point car)

(define y-point cdr)

(define a-point (make-point 1 1))
(define b-point (make-point 10 10))
(define segment (make-segment a-point b-point))

(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

(define (midpoint-segment s)
  (let ((start (start-segment s))
        (end (end-segment s)))
    (make-point (average (x-point start) (x-point end))
                (average (y-point start) (y-point end)))))

;(print-point (midpoint-segment segment))
;(newline)
