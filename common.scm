(define (even? n)
  (= (remainder n 2) 0))

(define (double n)
  (* n 2))

(define (halve n)
  (/ n 2))

(define (cube x)
  (* x x x))

(define (square x)
  (* x x))

(define (inc x)
  (+ x 1))

(define (identify x)
  x)

(define (average x y)
  (/ (+ x y) 2.0))
