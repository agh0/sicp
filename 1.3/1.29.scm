(load "../common.scm")
(load "chapter.scm")

(define (simpson f a b n)
  (define h (/ (- b a) n))
  (define (term k)
    (* (cond ((or (= k 0) (= k n)) 1)
             ((odd? k) 4)
             ((even? k) 2))
       (f (+ a (* k h)))))
  (define (next x) (+ x 1))
  (/ (* h (sum term 0 next n))
     3))

(display (simpson cube 0 1 100))
(newline)
;0.24999999999999992

(display (integral cube 0 1 0.01))
(newline)
;0.24998750000000042

(display (simpson cube 0 1 1000))
(newline)
;0.2500000000000003

(display (integral cube 0 1 0.001))
(newline)
;0.249999875000001
