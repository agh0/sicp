(load "../common.scm")

(define (double f)
  (lambda (x) (f (f x))))

(display ((double inc) 2))
(newline)

(display (((double (double double)) inc) 5))
(newline)
