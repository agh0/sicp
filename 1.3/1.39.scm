(load "../common.scm")
(load "1.37.scm")

(define (tan-cf x steps)
  (cont-frac (lambda (i) (if (= i 1) x
                             (- (square x))))
             (lambda (i) (- (* 2 i) 1))
             steps))

(display (tan (/ 3.14 3)))
(newline)

(display (tan-cf (/ 3.14 3) 12))
(newline)
