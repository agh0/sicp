; !

(define (filtered-accumulate combiner null-value term a next b filter)
  (if (> a b)
      null-value
      (combiner (if (filter a) (term a) null-value)
                (filtered-accumulate combiner null-value term (next a) next b filter))))

; a
(define (sum-prime-squares a b)
  (filtered-accumulate + 0 square a inc b prime?))

; b
(define (product-relatively-prime n)
  (define (relatively-to-n? k)
    (= 1 (gcd k n)))
  (filtered-accumulate * 1 identify 1 inc n relatively-to-n?))
