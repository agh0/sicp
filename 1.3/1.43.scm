(load "../common.scm")
(load "1.42.scm")

(define (repeated f times)
  (if (= times 1)
      f
      (compose f (repeated f (- times 1)))))

(display ((repeated square 2) 5))
(newline)
