(load "1.37.scm")

(define (e steps)
  (/ 1
     (cont-frac (lambda (i) 1.0)
                (lambda (i) (if (not (= (remainder i 3) 2))
                                     1
                                     (* 2 (/ (+ i 1) 3))))
                steps)))

(display (e 10))
(newline)
