(load "../common.scm")

; a
(define (product f next start end)
  (if (> start end)
      1
      (* (f start)
         (product f next (next start) end))))

(define (factorial n)
  (product identify inc 1 n))

;(display (factorial 5))
;(newline)

(define (pi n)
  (define (f x)
    (/ (* 4.0 (square x))
       (- (* 4.0 (square x)) 1)))
  (* 2.0
     (product f inc 1 n)))

;(display (pi 400))
;(newline)

; b
(define (product f next start end)
  (define (iter start result)
    (if (> start end)
        result
        (iter (next start) (* result (f start)))))
  (iter start 1))
