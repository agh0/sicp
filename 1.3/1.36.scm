(load "chapter.scm")

(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (display guess)
      (newline)
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(fixed-point (lambda (x) (/ (log 1000) (log x))) 10.0)

(newline)
(newline)

(fixed-point (lambda (x) (average x (/ (log 1000) (log x)))) 10.0)

(newline)
(newline)

;(fixed-point (lambda (y) (average y (/ 9 y))) 10.0)
