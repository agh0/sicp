(load "../common.scm")
(load "chapter.scm")

(define (cubic a b c)
  (lambda (x)
    (+ (cube x)
       (* a (square x))
       (* b x)
       c)))

(display (newtons-method (cubic 1 1 1) 1))
(newline)
