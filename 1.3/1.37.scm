; a
(define (cont-frac n d k)
  (define (recurse i)
    (/ (n i)
       (+ (d i)
          (if (< i k)
              (recurse (+ i 1))
              0))))
  (recurse 1))

(define (fi steps)
  (/ 1
     (cont-frac (lambda (i) 1.0)
                (lambda (i) 1.0)
                steps)))

;(display (fi 12))
;(newline)

; b
(define (cont-frac n d k)
  (define (iter i result)
    (if (= i 0)
        result
        (iter (- i 1)
              (/ (n i)
                 (+ (d i)
                    result)))))
  (iter k 0))

;(display (fi 12))
;(newline)
