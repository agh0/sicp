; !

(define (expmod base exp m)
  (remainder (fast-expt base exp) m))
(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))


(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

; После замеров выяснилось, что первый алгоритм вообще не хочет работать за вменяемое количество
; времени на тех же входных данных. Проблема в том, что этот алгоритм производит вычисления
; c числами много большими, чем m. Второй же, напротив, за m далеко не уходит (это отмечено
; в сноске авторами книги).
