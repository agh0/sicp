; !

(define (test n a)
   (= (expmod a n n) a))

(define (test-carmichael n)
  (test-all n 1))

(define (test-all n a)
  (cond ((= a n) true)
        ((not (test n a)) false)
        (else (test-all n (+ a 1)))))

(test-carmichael 561)
(test-carmichael 1105)
(test-carmichael 1729)
(test-carmichael 2465)
(test-carmichael 2821)
(test-carmichael 6601)
