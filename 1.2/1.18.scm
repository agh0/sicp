(load "../common.scm")

(define (fast-* a b)
    (fast-*-iter a b 0))

(define (fast-*-iter a b c)
    (cond ((= b 0) c)
        ((even? b) (fast-*-iter (double a) (halve b) c))
        (else (fast-*-iter a (- b 1) (+ a c)))))

(display (fast-* 120 13))
(newline)
