(load "../common.scm")

(define (fast-* a b)
    (cond ((= b 0) 0)
        ((even? b) (fast-* (double a) (halve b)))
        (else (+ a (fast-* a (- b 1))))))

(display (fast-* 200002 402019294))
(newline)
