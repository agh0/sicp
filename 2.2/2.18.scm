(define (reverse l)
  (if (null? l)
      l
      (append (reverse (cdr l)) (list (car l)))))

(display (reverse (list 1 4 9 16 25)))
