(define (same-parity first . rest)
  (define (test-parity a b)
    (= (remainder a 2) (remainder b 2)))
  (define (filter-parity nums)
    (if (null? nums)
        nums
        (if (test-parity first (car nums))
            (cons (car nums) (filter-parity (cdr nums)))
            (filter-parity (cdr nums)))))
  (cons first (filter-parity rest)))

(display (same-parity 1 2 3 4 5 6 7))
(newline)

(display (same-parity 2 3 4 5 6 7))
(newline)
